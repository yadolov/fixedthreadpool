﻿using System;
using System.Collections.Generic;
using System.Threading;

using ThreadPools;

namespace ThreadPoolsTest
{
    /// <summary>Тестовая задача.
    /// Используется sleep для имитации работы</summary>
    internal sealed class SleepTask : ITask
    {
        /// <summary>Конструктор</summary>
        /// <param name="id">Идентификатор задачи</param>
        /// <param name="priority">Приоритет задачи</param>
        /// <param name="timeToSleep">Время выполнения</param>
        public SleepTask(string id, Priority priority, int timeToSleep)
        {
            m_Id = id;
            m_Priority = priority;
            m_TimeToSleep = timeToSleep;
        }

        /// <summary>Приоритет задачи</summary>
        public Priority Priority
        {
            get { return m_Priority; }
        }

        #region Реализация ITask

        /// <summary>Выполнить задачу</summary>
        public void Execute()
        {
            Console.WriteLine("Thread {0}: Task {1} {2} => Start",
                Thread.CurrentThread.ManagedThreadId, m_Id, m_Priority);
            Thread.Sleep(m_TimeToSleep);
            Console.WriteLine("Thread {0}: Task {1} {2} => Stop",
                Thread.CurrentThread.ManagedThreadId, m_Id, m_Priority);
        }

        #endregion

        /// <summary>Идентификатор задачи</summary>
        private readonly string m_Id;
        /// <summary>Приоритет задачи</summary>
        private readonly Priority m_Priority;
        /// <summary>Время выполнения</summary>
        private readonly int m_TimeToSleep;
    }

    /// <summary>Генератор задач со случайным приоритетом и временем выполнения</summary>
    internal sealed class TaskFuzzyProducer : IEnumerable<SleepTask>
    {
        /// <summary>Конструктор</summary>
        /// <param name="taskIdPrefix">Префикс идентификатора задачи</param>
        /// <param name="minTimeToSleep">Минимальное время выполнения</param>
        /// <param name="maxTimeToSleep">Максимальное время выполнения</param>
        public TaskFuzzyProducer(string taskIdPrefix,
            int minTimeToSleep, int maxTimeToSleep)
        {
            m_TaskIdPrefix = taskIdPrefix;
            m_MinTimeToSleep = minTimeToSleep;
            m_MaxTimeToSleep = maxTimeToSleep;
        }

        /// <summary>Конструктор</summary>
        /// <param name="taskIdPrefix">Префикс идентификатора задачи</param>
        /// <param name="minTimeToSleep">Минимальное время выполнения</param>
        /// <param name="maxTimeToSleep">Максимальное время выполнения</param>
        /// <param name="prioritySeed">Идентификатор последовательности
        /// случайных приоритетов</param>
        /// <param name="timeToSleepSeed">Идентификатор последовательности
        /// случайных времен выполнения</param>
        public TaskFuzzyProducer(string taskIdPrefix,
            int minTimeToSleep, int maxTimeToSleep,
            int prioritySeed, int timeToSleepSeed) :
            this(taskIdPrefix, minTimeToSleep, maxTimeToSleep)
        {
            m_PrioritySeed = prioritySeed;
            m_TimeToSleepSeed = timeToSleepSeed;
        }

        #region Реализация IEnumerable

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        #endregion

        #region Реализация IEnumerable<ITask>

        public IEnumerator<SleepTask> GetEnumerator()
        {
            return new TaskFuzzyProducerEnm(
                m_TaskIdPrefix, m_MinTimeToSleep, m_MaxTimeToSleep,
                m_PrioritySeed, m_TimeToSleepSeed);
        }

        #endregion

        private sealed class TaskFuzzyProducerEnm : IEnumerator<SleepTask>
        {
            public TaskFuzzyProducerEnm(string taskIdPrefix,
                int minTimeToSleep, int maxTimeToSleep)
            {
                m_TaskIdFormat = string.Format("{0}{{0}}", taskIdPrefix);
                Array priorities = Enum.GetValues(typeof(Priority));
                m_MinPriority = (int)priorities.GetValue(0);
                m_MaxPriority = (int)priorities.GetValue(priorities.Length - 1) + 1;
                m_MinTimeToSleep = minTimeToSleep;
                m_MaxTimeToSleep = maxTimeToSleep + 1;
                Reset();
            }

            public TaskFuzzyProducerEnm(string taskIdPrefix,
                int minTimeToSleep, int maxTimeToSleep,
                int prioritySeed, int timeToSleepSeed) :
                this(taskIdPrefix, minTimeToSleep, maxTimeToSleep)
            {
                m_PrioritySeed = prioritySeed;
                m_TimeToSleepSeed = timeToSleepSeed;
            }

            #region Реализация IDisposable

            public void Dispose()
            {
            }

            #endregion

            #region Реализация IEnumerator

            object System.Collections.IEnumerator.Current
            {
                get { return Current; }
            }

            public bool MoveNext()
            {
                ++m_CurrentTaskNum;
                m_CurrentTask = new SleepTask(
                    string.Format(m_TaskIdFormat, m_CurrentTaskNum),
                    (Priority)m_RandomPriority.Next(m_MinPriority, m_MaxPriority),
                    m_RandomTimeToSleep.Next(m_MinTimeToSleep, m_MaxTimeToSleep));
                return true;
            }

            public void Reset()
            {
                m_CurrentTaskNum = 0;
                m_RandomPriority = new Random(m_PrioritySeed);
                m_RandomTimeToSleep = new Random(m_TimeToSleepSeed);
                m_CurrentTask = null;
            }

            #endregion

            #region Реализация IEnumerator<ITask>

            public SleepTask Current
            {
                get
                {
                    if (m_CurrentTaskNum == 0)
                        throw new InvalidOperationException();
                    return m_CurrentTask;
                }
            }

            #endregion

            private readonly string m_TaskIdFormat;
            private readonly int m_MinPriority;
            private readonly int m_MaxPriority;
            private readonly int m_MinTimeToSleep;
            private readonly int m_MaxTimeToSleep;
            private readonly int m_PrioritySeed;
            private readonly int m_TimeToSleepSeed;
            private Random m_RandomPriority;
            private Random m_RandomTimeToSleep;
            private UInt64 m_CurrentTaskNum;
            private SleepTask m_CurrentTask;
        }

        private readonly string m_TaskIdPrefix;
        private readonly int m_MinTimeToSleep;
        private readonly int m_MaxTimeToSleep;
        private readonly int m_PrioritySeed;
        private readonly int m_TimeToSleepSeed;
    }
}
