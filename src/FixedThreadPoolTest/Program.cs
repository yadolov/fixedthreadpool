﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

using NUnit.Framework;

using ThreadPools;

namespace ThreadPoolsTest
{
    class Program
    {
        static void Main(string[] args)
        {
            Thread testThread = new Thread(DoSleepFuzzyTest);
            testThread.Start();
            Console.WriteLine("Press any ket to quit.");
            Console.ReadKey();
            Console.WriteLine("Quitting...");
            m_StopExecution.Set();
            testThread.Join();
        }

        /// <summary>Выполнение задач со случайным приоритетом и временем выполнения</summary>
        private static void DoSleepFuzzyTest()
        {
            var threadPool = new FixedThreadPool(5);
            ParallelOptions parallelOptions = new ParallelOptions();
            parallelOptions.MaxDegreeOfParallelism = 5;
            var taskProducerEnum1 = new TaskFuzzyProducer("", 500, 3500);
            Parallel.ForEach<SleepTask>(taskProducerEnum1, parallelOptions,
                (SleepTask task, ParallelLoopState state) =>
                {
                    Thread.Sleep(500);
                    if (m_StopExecution.WaitOne(0))
                    {
                        threadPool.Stop();
                        state.Break();
                    }
                    threadPool.Execute(task, task.Priority);
                });
        }

        /// <summary>Событие необходимости остановки выполнения</summary>
        private static readonly ManualResetEvent m_StopExecution =
            new ManualResetEvent(false);
    }

    /// <summary>Набор NUnit-тестов для PriorityQueue</summary>
    [TestFixture]
    public class PriorityQueueTest
    {
        [Test]
        public void DoPriorityQueueIsEmptyTest()
        {
            var queue = new PriorityQueue<int>();
            Assert.AreEqual(queue.IsEmpty, true);
            queue.Enqueue(1, Priority.NORMAL);
            queue.Enqueue(2, Priority.HIGH);
            queue.Enqueue(3, Priority.LOW);
            Assert.AreEqual(queue.IsEmpty, false);
            queue.Dequeue();
            queue.Dequeue();
            queue.Dequeue();
            Assert.AreEqual(queue.IsEmpty, true);
        }

        [Test]
        public void DoPriorityQueueHighNormalLowTest()
        {
            var queue = new PriorityQueue<int>();
            queue.Enqueue(1, Priority.LOW);
            queue.Enqueue(2, Priority.NORMAL);
            queue.Enqueue(3, Priority.HIGH);
            Assert.AreEqual(queue.Dequeue(), 3);
            Assert.AreEqual(queue.Dequeue(), 2);
            Assert.AreEqual(queue.Dequeue(), 1);
        }

        [Test]
        public void DoPriorityQueueGroupingTest()
        {
            var queue = new PriorityQueue<int>();
            queue.Enqueue(1, Priority.NORMAL);
            queue.Enqueue(2, Priority.HIGH);
            queue.Enqueue(3, Priority.HIGH);
            queue.Enqueue(4, Priority.HIGH);
            queue.Enqueue(5, Priority.HIGH);
            Assert.AreEqual(queue.Dequeue(), 2);
            Assert.AreEqual(queue.Dequeue(), 3);
            Assert.AreEqual(queue.Dequeue(), 4);
            Assert.AreEqual(queue.Dequeue(), 1);
            Assert.AreEqual(queue.Dequeue(), 5);
        }

        [Test]
        [ExpectedException("System.InvalidOperationException")]
        public void DoPriorityQueueEmptyDequeueTest()
        {
            var queue = new PriorityQueue<int>();
            queue.Dequeue();
        }
    }
}
