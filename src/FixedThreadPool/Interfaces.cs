﻿namespace ThreadPools
{
    /// <summary>Приоритет</summary>
    public enum Priority
    {
        /// <summary>Нормальный</summary>
        NORMAL = 0,
        /// <summary>Высокий</summary>
        HIGH,
        /// <summary>Низкий</summary>
        LOW
    }

    /// <summary>Задача</summary>
    public interface ITask
    {
        /// <summary>Выполнить задачу</summary>
        void Execute();
    }

    /// <summary>Фиксированный по размеру пул потоков</summary>
    public interface IFixedThreadPool
    {
        /// <summary>Выполнить задачу с заданным приоритетом</summary>
        /// <param name="task">Задача</param>
        /// <param name="priority">Приоритет</param>
        /// <returns>false - выполнение задач остановлено</returns>
        bool Execute(ITask task, Priority priority);
        /// <summary>Остановить выполнение всех задач</summary>
        void Stop();
    }

    /// <summary>Очередь с сортировкой элементов по приоритету</summary>
    /// <typeparam name="T">Тип элемента очереди</typeparam>
    internal interface IPriorityQueue<T>
    {
        /// <summary>Добавить элемент с приоритетом</summary>
        /// <param name="item">Элемент</param>
        /// <param name="priority">Приоритет</param>
        void Enqueue(T item, Priority priority);
        /// <summary>Извлечь элемент</summary>
        /// <returns>Элемент очереди</returns>
        T Dequeue();
        /// <summary>Признак пустой очереди</summary>
        /// <returns>true - очередь пуста</returns>
        bool IsEmpty { get; }
    }
}
