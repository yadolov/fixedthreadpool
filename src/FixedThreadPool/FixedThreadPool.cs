﻿using System.Collections.Generic;
using System.Threading;

namespace ThreadPools
{
    /// <summary>Фиксированный по размеру пул потоков</summary>
    public class FixedThreadPool : IFixedThreadPool
    {
        /// <summary>Конструктор</summary>
        /// <param name="threadMaxCount">Количество потоков в пуле</param>
        public FixedThreadPool(int threadMaxCount)
        {
            Thread[] threadPool = new Thread[threadMaxCount];
            m_ThreadPool = threadPool;
            m_ThreadPoolParams = new ThreadPoolParams[threadMaxCount];
            m_ThreadPoolReady = new WaitHandle[threadMaxCount];
            for (int threadNdx = 0; threadNdx < threadMaxCount; ++threadNdx)
            {
                Thread thread = new Thread(ExecuteTasks);
                ThreadPoolParams prms = new ThreadPoolParams(
                    new AutoResetEvent(false), new AutoResetEvent(false));
                threadPool[threadNdx] = thread;
                m_ThreadPoolParams[threadNdx] = prms;
                m_ThreadPoolReady[threadNdx] = prms.ThreadReady;
                thread.Start(prms);
            }
            m_ShedulerThread = new Thread(ScheduleTasks);
            m_ShedulerThread.Start();
        }

        #region Реализация IFixedThreadPool

        /// <summary>Выполнить задачу с заданным приоритетом</summary>
        /// <param name="task">Задача</param>
        /// <param name="priority">Приоритет</param>
        /// <returns>false - выполнение задач остановлено</returns>
        /// <remarks>Задача добавляется в очередь на выполнение
        /// с заданным приоритетом</remarks>
        public bool Execute(ITask task, Priority priority)
        {
            if (m_StopShedulerExecution.WaitOne(0))
                return false;
            lock (m_TaskQueueLock)
            {
                m_TaskQueue.Enqueue(task, priority);
            }
            m_NewTask.Set();
            return true;
        }

        /// <summary>Остановить выполнение всех задач</summary>
        /// <remarks>Ожидает выполнения всех текущих задач</remarks>
        public void Stop()
        {
            m_StopShedulerExecution.Set();
            m_ShedulerThread.Join();
        }

        #endregion

        /// <summary>Раздача задач по рабочим потокам</summary>
        private void ScheduleTasks()
        {
            WaitHandle[] events =
                new WaitHandle[] { m_StopShedulerExecution, m_NewTask };
            while (true)
            {
                // Определяем необходимость завершения работы планировщика
                bool exit = WaitHandle.WaitAny(events) == 0;
                // Раздаем рабочим потокам все задачи из очереди
                while (true)
                {
                    ITask task = null;
                    lock (m_TaskQueueLock)
                    {
                        if (m_TaskQueue.IsEmpty)
                            break;
                        task = m_TaskQueue.Dequeue();
                    }
                    // Ожидаем свободный рабочий поток
                    int threadNdx =
                        WaitHandle.WaitAny(m_ThreadPoolReady);
                    // Назначаем потоку задачу и
                    // сигнализируем ему о необходимости ее выполнения
                    ThreadPoolParams prms =
                        m_ThreadPoolParams[threadNdx];
                    prms.Task = task;
                    prms.ExecuteTask.Set();
                }
                if (exit)
                {
                    // Завершаем работу рабочих потоков и выходим
                    m_StopThreadPoolExecution.Set();
                    foreach (Thread thread in m_ThreadPool)
                    {
                        thread.Join();
                    }
                    return;
                }
            }
        }

        /// <summary>Выполнение задач</summary>
        /// <param name="workerThreadParamsObj">Параметры выполнения</param>
        private void ExecuteTasks(object workerThreadParamsObj)
        {
            ThreadPoolParams prms =
                workerThreadParamsObj as ThreadPoolParams;
            WaitHandle[] events =
                new WaitHandle[] { m_StopThreadPoolExecution, prms.ExecuteTask };
            while (true)
            {
                prms.ThreadReady.Set();
                switch (WaitHandle.WaitAny(events))
                {
                    case 0: // Необходимо завершить работу
                        return;
                    case 1: // Необходимо выполнить задачу
                        {
                            ITask task = prms.Task;
                            prms.Task = null;
                            if (task != null)
                                task.Execute();
                        }
                        break;
                }
            }
        }

        /// <summary>Параметры выполнения рабочего потока</summary>
        internal sealed class ThreadPoolParams
        {
            /// <summary>Конструктор</summary>
            /// <param name="threadReady">Событие готовности рабочего потока</param>
            /// <param name="executeTask">Событие необходимости выполнения задачи</param>
            public ThreadPoolParams(AutoResetEvent threadReady,
                AutoResetEvent executeTask)
            { 
                m_ThreadReady = threadReady;
                m_ExecuteTask = executeTask;
            }

            /// <summary>Событие готовности рабочего потока</summary>
            public AutoResetEvent ThreadReady
            {
                get { return m_ThreadReady; }
            }

            /// <summary>Событие необходимости выполнения задачи</summary>
            public AutoResetEvent ExecuteTask
            {
                get { return m_ExecuteTask; }
            }

            /// <summary>Задача</summary>
            public ITask Task
            {
                get { return m_Task; }
                set { m_Task = value; }
            }

            /// <summary>Событие готовности рабочего потока</summary>
            private readonly AutoResetEvent m_ThreadReady;
            /// <summary>Событие необходимости выполнения задачи</summary>
            private readonly AutoResetEvent m_ExecuteTask;
            /// <summary>Задача</summary>
            private ITask m_Task;
        }

        /// <summary>Очередь задач</summary>
        private readonly IPriorityQueue<ITask> m_TaskQueue =
            new PriorityQueue<ITask>();
        /// <summary>Объект синхронизации доступа к очереди задач</summary>
        private readonly object m_TaskQueueLock =
            new object();
        /// <summary>Событие поступления новых задач</summary>
        private readonly AutoResetEvent m_NewTask =
            new AutoResetEvent(false);
        /// <summary>Поток-планировщик выполнения задач</summary>
        private readonly Thread m_ShedulerThread;
        /// <summary>Событие завершения работы потока-планировщика
        /// выполнения задач</summary>
        private readonly ManualResetEvent m_StopShedulerExecution =
            new ManualResetEvent(false);
        /// <summary>Перечисление рабочих потоков</summary>
        private readonly IEnumerable<Thread> m_ThreadPool;
        /// <summary>Параметры выполнения рабочих потоков</summary>
        private readonly ThreadPoolParams[] m_ThreadPoolParams;
        /// <summary>События готовности рабочих потоков</summary>
        private readonly WaitHandle[] m_ThreadPoolReady;
        /// <summary>Событие завершения работы пула рабочих потоков</summary>
        private readonly ManualResetEvent m_StopThreadPoolExecution =
            new ManualResetEvent(false);
    }
}