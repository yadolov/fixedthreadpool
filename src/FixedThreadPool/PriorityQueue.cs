﻿using System;
using System.Collections.Generic;

namespace ThreadPools
{
    /// <summary>Очередь с сортировкой элементов по приоритету.
    /// Для очереди действуют следующие правила:
    /// - на три задачи с приоритетом HIGH
    /// выполняется одна задача с приоритетом NORMAL,
    /// - задачи с приоритетом LOW не выполняются,
    /// пока есть хоть одна задача с другим приоритетом.</summary>
    /// <typeparam name="T">Тип элемента очереди</typeparam>
    /// <remarks>Для многопоточного доступа требуется внешняя синхронизация</remarks>
    internal class PriorityQueue<T> : IPriorityQueue<T>
    {
        #region Реализация IPriorityQueue<T>

        /// <summary>Добавить элемент с приоритетом</summary>
        /// <param name="item">Элемент</param>
        /// <param name="priority">Приоритет</param>
        /// <exception cref="System.NotSupportedException">
        /// Генерируется в случае если добавляется элемент
        /// с неподдерживаемым приоритетом</exception>
        public void Enqueue(T item, Priority priority)
        {
            switch (priority)
            {
                case Priority.NORMAL:
                case Priority.HIGH:
                    var itemQueue = priority == Priority.NORMAL ?
                        m_NormalPriorityQueue : m_HighPriorityQueue;
                    itemQueue.Enqueue(item);
                    // Если есть возможность создать группу элементов
                    // Высокий-Высокий-Высокий-Нормальный, создаем ее
                    // и добавляем в комбинированную очередь
                    if ((m_NormalPriorityQueue.Count != 0)
                        && (m_HighPriorityQueue.Count > 2))
                    {
                        var itemGroup = new Queue<T>(3);
                        itemGroup.Enqueue(m_HighPriorityQueue.Dequeue());
                        itemGroup.Enqueue(m_HighPriorityQueue.Dequeue());
                        itemGroup.Enqueue(m_HighPriorityQueue.Dequeue());
                        itemGroup.Enqueue(m_NormalPriorityQueue.Dequeue());
                        m_CombinedPriorityQueue.Enqueue(itemGroup);
                    }
                    break;
                case Priority.LOW:
                    m_LowPriorityQueue.Enqueue(item);
                    break;
                default:
                    throw new NotSupportedException("priority");
            }
        }

        /// <summary>Извлечь элемент</summary>
        /// <returns>Элемент очереди</returns>
        /// <exception cref="System.InvalidOperationException">
        /// Генерируется в случае если очередь пуста</exception>
        public T Dequeue()
        {
            if (m_CombinedPriorityQueue.Count != 0)
            {
                var itemGroup = m_CombinedPriorityQueue.Peek();
                var item = itemGroup.Dequeue();
                if (itemGroup.Count == 0)
                    m_CombinedPriorityQueue.Dequeue();
                return item;
            }
            if (m_HighPriorityQueue.Count != 0)
            {
                return m_HighPriorityQueue.Dequeue();
            }
            if (m_NormalPriorityQueue.Count != 0)
            {
                return m_NormalPriorityQueue.Dequeue();
            }
            if (m_LowPriorityQueue.Count != 0)
            {
                return m_LowPriorityQueue.Dequeue();
            }
            throw new InvalidOperationException("Queue is empty");
        }

        /// <summary>Признак пустой очереди</summary>
        /// <returns>true - очередь пуста</returns>
        public bool IsEmpty
        { 
            get
            {
                return (m_CombinedPriorityQueue.Count == 0) &&
                    (m_LowPriorityQueue.Count == 0) &&
                    (m_NormalPriorityQueue.Count == 0) &&
                    (m_HighPriorityQueue.Count == 0);
            }
        }

        #endregion

        /// <summary>Очередь задач с низким приоритетом</summary>
        private readonly Queue<T> m_LowPriorityQueue = new Queue<T>();
        /// <summary>Очередь задач с нормальным приоритетом</summary>
        private readonly Queue<T> m_NormalPriorityQueue = new Queue<T>();
        /// <summary>Очередь задач с высоким приоритетом</summary>
        private readonly Queue<T> m_HighPriorityQueue = new Queue<T>();
        /// <summary>Очередь задач со смешанным приоритетом</summary>
        private readonly Queue<Queue<T>> m_CombinedPriorityQueue = new Queue<Queue<T>>();
    }
}
